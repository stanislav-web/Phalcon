Phalcon Development Boards
-----------------------
###### An example development of the administrative part of the application by Phalcon Framework.

[![Build Status](https://travis-ci.org/stanislav-web/Phalcon-development.svg)](https://travis-ci.org/stanislav-web/Phalcon-development) [![Dependency Status](https://www.versioneye.com/user/projects/54e5c671b3ca9b5b72000165/badge.svg?style=flat)](https://www.versioneye.com/user/projects/54e5c671b3ca9b5b72000165) [![Latest Stable Version](https://poser.pugx.org/stanislav-web/phalcon-development/v/stable.svg)](https://packagist.org/packages/stanislav-web/phalcon-development) [![Total Downloads](https://poser.pugx.org/stanislav-web/phalcon-development/downloads.svg)](https://packagist.org/packages/stanislav-web/phalcon-development) [![License](https://poser.pugx.org/stanislav-web/phalcon-development/license.svg)](https://packagist.org/packages/stanislav-web/phalcon-development)
-----------------------
![Alt text](http://hsto.org/storage2/f65/3fa/800/f653fa800c35d29e02253b3ab578b99c.png "Phalcon") ![Alt text](http://mgcrea.github.io/angular-7min/images/angularjs.png "Angular") 

#### Preface
* Backend (Phalcon, jQuery, Twitter Bootstrap)
* Frontend (Phalcon, jQuery, AngularJS, Twitter Bootstrap)

#### Implemented
* Staging development
* Service layer
* Phalcon Crypt / Decrypt cookies
* Phalcon + Angular JS token + cookies authentication

#### Requirements 
* Requires PHP 5.4
* Phalcon 1.3.3
* Memcache
* APC (APCu)
* PHP mcrypt library
* PHP PDO MySQL driver

#### Testing 
* PHP 5.5.9 (with FPM)
* MySQL 5.7.15
* libmemcached 1.0.8

#### Documents
+ [Документация Phalcon 1.3.0](http://docs.phalconphp.com/ru/latest/index.html)
+ [Guide to AngularJS Documentation](https://docs.angularjs.org/guide)
+ [Защита веб-приложения на Phalcon + AngularJS от CSRF атак](http://habrahabr.ru/post/245467/)
+ [Combinar Angular y Phalcon](http://uno-de-piera.com/combinar-angular-y-phalcon/)
+ [The Basics of JavaScript Framework SEO in AngularJS](http://builtvisible.com/javascript-framework-seo/)
+ [Angular Translate](http://angular-translate.github.io/)
+ [Languages for Bootstrap 3](http://usrz.github.io/bootstrap-languages/)
+ [SEXY SPLASH MODAL](http://popdevelop.com/2014/07/sexy-splash-modal-using-bootstrap-css3-and-angularjs/)

------------------------
![Alt text](http://dl1.joxi.net/drive/0001/0378/90490/141130/6931035855.jpg "Screen")
![Angular + Phalcon routing](http://dl1.joxi.net/drive/0001/0378/90490/150110/32cfed48dd.jpg "Angular + Phalcon routing")
![Angular + Phalcon routing](http://dl2.joxi.net/drive/0004/0211/323795/150114/fe9907631e.jpg "Angular + Phalcon routing")
![Angular + Splash auth](http://dl1.joxi.net/drive/0004/0211/323795/150117/650a8c8cc1.jpg "Angular + Phalcon routing")
